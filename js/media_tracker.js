﻿(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.mediaTracker = {
    attach: function (context, settings) {
      $('audio, video', context).once('media-tracker').each(function () {
        let media_id = $(this).attr('trackable_media_id');

        if (media_id) {
          $(this).on('playing', {'media_id': media_id}, increasePlayCount);
        }
      });

      /**
       * Send the Ajax request to increase play counter.
       *
       * We store in the local storage id of a media that was played.
       * We need this in order to not increase the counter if a user plays
       * the track more than once. We check if the local storage contains id of
       * a media, and if yes, then we don't send a request to increase play counter.
       */
      function increasePlayCount(e) {
        const media_id = e.data.media_id;
        let played_media_ids = [];

        if (localStorage.getItem('played_media_ids')) {
          played_media_ids = JSON.parse(localStorage.getItem('played_media_ids'));
        }

        if (played_media_ids.includes(media_id)) {
          return;
        }

        const endpoint = '/increase-play-counter/' + media_id;
        const ajax = new Drupal.Ajax(false, false, {url: endpoint});

        ajax.execute().done(function () {
          // Store a media id to prevent redundant increasing of play counter
          // if the user will play this media repeatedly.
          played_media_ids.push(media_id);
          localStorage.setItem('played_media_ids', JSON.stringify(played_media_ids));
        });
      }
    }
  };

})(jQuery, Drupal);
