<?php

namespace Drupal\media_tracker\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Core\Ajax\AjaxResponse;

/**
 * Increases the play media counter and updates the 'field_play_counter'.
 */
class MediaTrackerController extends ControllerBase {

  /**
   * EntityTypeManager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * State service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->state = $container->get('state');

    return $instance;
  }

  /**
   * Check if a user has played a media, and if not then increase play counter.
   *
   * If a user plays a media several times, it will not increment the counter.
   * To prevent re-summing, we store IP addresses of anonymous users
   * and ids of authenticated users.
   *
   * @param string $media_id
   *   A media id.
   * @param Symfony\Component\HttpFoundation\Request $request
   *   Request object.
   *
   * @return Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function increasePlayCounter($media_id, Request $request) {

    if (!$request->isXmlHttpRequest()) {
      throw new NotFoundHttpException();
    }

    if ($this->currentUser()->isAnonymous()) {
      $ip = $request->getClientIp();
      $key = "media_tracker.{$media_id}.ips";
      // Array of IP addresses of users who played a media with id = $media_id.
      $ips = $this->state->get($key) ?? [];

      if (!in_array($ip, $ips)) {
        $this->updateMediaCounter($media_id);
        $ips[] = $ip;
        $this->state->set($key, $ips);
      }
    }
    else {
      $uid = $this->currentUser()->id();
      $key = "media_tracker.{$media_id}.uids";
      // Array of ids of authenticated users who played a media.
      $uids = $this->state->get($key) ?? [];

      if (!in_array($uid, $uids)) {
        $this->updateMediaCounter($media_id);
        $uids[] = $uid;
        $this->state->set($key, $uids);
      }
    }

    return new AjaxResponse();
  }

  /**
   * Increase play counter and update the 'field_play_counter' of media entity.
   *
   * @param string $media_id
   *   A media id.
   */
  private function updateMediaCounter($media_id) {
    $media = $this->entityTypeManager()->getStorage('media')->load($media_id);
    $counter = $media->field_play_counter->value ?? 0;
    $counter++;
    $media->set('field_play_counter', $counter);
    $media->save();
  }

}
